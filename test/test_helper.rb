require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'test_helper'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all
  include ApplicationHelper
  # Add more helper methods to be used by all tests here...
  
# return true if a test user logged in
  def is_logged_in?
  	!session[:user_id].nil?
  end

# log in as a particular user
	def log_in_as(user)
		session[:user_id] = user.id
	end
end

class ActionDispatch::IntegrationTest
	def log_in_as(user, password: 'password', remenber_me: '1')
		post login_path, params: { session: { email: user.email,
												password: password,
												remenber_me: remenber_me
		} }
	end
end
