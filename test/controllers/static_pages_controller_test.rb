require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  test "should get letopo" do
    get about_path
    assert_response :success
    assert_select "title",  "letopo"
  end

  test "should get home" do
    get home_path
    assert_response :success
    assert_select "title",  "home"
  end

  test "should get fabrique" do
    get fabrique_path
    assert_response :success
    assert_select "title",  "fabrique"
  end

    test "should get causeries" do
    get causeries_path
    assert_response :success
    assert_select "title",  "causeries"
  end
end

