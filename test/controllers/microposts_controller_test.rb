require 'test_helper'

# class MicropostsControllerTest < ActionDispatch::IntegrationTest
#   # test "the truth" do
#   #   assert true
#   # end
#   def setup
#   	@micropost = microposts(:orange)
#   end

#   test "should redirect create when not logged in" do
#   	assert_no_difference, 'Micropost.count' do
#   		post micropost_path, params: { micropost: { content: "Lorem ipsum"}}
#   	end
#   	assert_redirect_to login_url
#   end

#   test "should redirect destroy when not logged in" do
#   	assert_no_difference 'Micropost.count' do
#   		delete micropost_path(@micropost)
#   	end
#   	assert_redirect_to login_url
#   end

# # to see who can delete: for orama
#   test "should redirect destroy for wrong micropost" do
#   	log_in_as(users(:newone))
#   	microposts = microposts(:ants)
#   	assert_no_difference 'Micropost.count' do
#   		delete micropost_path(micropost)
#   	end
#   	assert_redirect_to root_url
#   end
# end
