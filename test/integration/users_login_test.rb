require 'test_helper'

class UserLoginTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = users(:newone)
  end

  test "login with valid information follow by logout" do 
    get login_path
    post login_path, params: { session: {email: @user.email, password: 'password'}}
    assert is_logged_in?
    assert_redirected_to @user
    follow_redirect!
    assert_template 'users/show'
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", user_path(@user)
    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_url
    delete logout_path
    follow_redirect
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path, count: 0
    assert_select "a[href=?]", user_path(@user), count: 0
  end

  test "login whith remenbering" do
    log_in_as(@user, remenber_me: '1')
    assert_equal FILL_IN, assigns(:user).FILL_IN
  end

  test "login without remenbering" do
    log_in_as(@user, remenber_me: '1')
    log_in_as(@user, remenber_me: '0')
    assert_empty cookies['remenber_token']
  end

#  test "login whith invalid information" do
#  		get login_path
#  		assert_template 'session/new'
#  		post login_path, params: {session: {email: "", password: ""}}
#  		assert_template 'session/new'
#  		assert_not flash.empty?
#  		get root_path
 # 		assert flash.empty?
 # end
end
