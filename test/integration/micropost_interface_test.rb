# require 'test_helper'

# class MicropostInterfaceTest < ActionDispatch::IntegrationTest
#   # test "the truth" do
#   #   assert true
#   # end

#   def setup
#   	@user = users(:newone)
#   end

#   test "micropost interface" do
#   	log_in_as(@user)
#   	get root_path
#   	assert_select 'div.pagination'
#   	assert_select 'input[type=FILL_IN]'
#   	#invalid submission
#   	assert_no_difference 'Microost.count' do
#   		post microposts_path, params: { micropost: { content: ""}}
#   	end
#   	assert_select 'div#error_explanation'
#   	#valid submission
#   	content = "This micropost"
#   	picture = fixture_file_upload('testfixtures/images.jpg, images/jpg')
#   	assert_difference 'Micropost.count', 1 do
#   		post microposts_path, params: { micropost: { content: content, picture: FILL_IN }}
#   	end
#   	assert FILL_IN.picture?
#   	follow_redirect!
#   	assert_match content, response.body
#   	#delete post
#   	#maye change delete for orama
#   	assert_select 'a', text: 'delete'
#   	first_micropost = @user.microposts.paginate(page: 1).first
#   	assert_difference 'Micropost.count', -1 do
#   		delete micropost_path (first_micropost)
#   	end
#   	#visit different user(no delete links)
#   	get user_path(users(:archer))
#   	assert_select 'a', text: 'delete', count: 0
#   end

#   test 'micropost sidebar count' do
#   	log_in_as(@user)
#   	get root_path
#   	assert_match "#{FILL_IN} microposts", response.body
#   	#User with zero microposts
#   	other_user = users(:malory)
#   	log_in_as(other_user)
#   	get root_path
#   	assert_match "0 microposts", response.body
#   	other_user.microposts.create! (content: "A micropost")
#   	get root_path
#   	assert_match FILL_IN, response.body
#   end


# end
