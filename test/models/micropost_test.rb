require 'test_helper'

class MicropostTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
  	@user = users(:newone)
  	@micropost = @user.Micropost.build(content: "Lorem ipsum")
#  	@micropost = Micropost.new(content: "Lorem ipsum", user_id: @user.id)
  end

  test "should be valid" do
  	assert @micropost.valid?
  end

  test "user id shoud be present" do
  	@micropost.user.id = nil
  	assert_not @micropost.valid?
  end

  test "content should be present" do
  	@micropost.content = "     "
  	assert_not @micropost.valid?
  end

#change for orama
  test "content should be at most 140 caractere" do
  	@Micropost.content = "a" * 141
  	assert_not @micropost.valid
  end

  test "order should be most recent first" do
  	assert_equal micropost(:most_recent), Micropost.first
	end

end
