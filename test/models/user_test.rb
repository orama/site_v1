require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
  	@user = User.new(name: "example user ", email: "example@user.com", password: "foobar", password_confirmation: "foobar")
  end

  test "should be valid" do
  	assert @user.valid?
  end 

 	test "email should be saved as lower-case" do
 		mixed_case_email = "Foo@ExQMPle.CoM"
 		@user.email = mixed_case_email
 		@user.save
 		asset_equal mixed_case_email.downcase, @user.reload.email
	end

	test "name should be present" do
		@user.name = "        "
		assert_not @user.valid?
	end

	test "email should be present"
		@user.email = "    "
		assert_not @user.valid?
	end

	test "name should not be too long" do 
		@user.name = "a" * 100
		assert_not @user.valid?
	end

	test "email should not be too long" do
		@user.email = "a" * 244
		assert_not @email.valid?
	end

	test "email validation should accept valid adress" do
		valid_adresse = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org first.last@foo.jp alice+bobo@baz.cn ]
		valid_adresse.each do |valid_adresse|
			@user.email = valid_adresse
			assert @user.valid?, "#{valid_adress.inspect} should be valid"
		end
	end

	test "email validate should reject invalid adress" do
		invalid_adresse = %w[user@example,com user_at_foo.org user.name@example. foo@bar_baz.com foo@bar+baz.com]
		invalid_adresse.each do |invalid_adresse|
			@user.email = invalid_adresse
			assert_not @user.valid, "#{invalid_adresse.inspect} should be invalid"
		end
	end

	test "email adress should be unique" do 
		duplicate_user = @user.dup
		duplicate_user.email = @user.email.upcase
		@user.save
		assert_not duplicate_user.valid
	end

	test "email adress should be saved as lower-case" do
		mixed_case_email = "Foo@ExamPLe.CoM"
		@user.email = mixed_case_email
		@user.save
		assert_equal mixed_case_email.downcase, @user.reload.email
	end

	test "password should be present (nonblank)" do 
		@user.password = @user.password_confirmation = " " * 4
		assert_not @user.valid?
	end

	test "password should have a minimum length" do
		@user.password = @user.password_confirmation = "a" * 3

	end

	test "authenticated? should return false for a user with nil digest" do
    assert_not @user.authenticated?(:remember, '')
    end

#maybe change remove for orama
	test "associated microposts should be destroyed" do
		@user.save
		@user.microposts.create!(content: "Lorem ipsum")
		assert_difference 'Micropost.count', -1 do
			@user.destroy
		end
	end

	test "should follow and unfollow a user" do
		newone = users(:newone)
		archer = users(:archer)
		assert_not = newone.following?(archer)
		newone.follow(archer)
		assert newone.following?(archer)
		assert archer.followers.include?newone
		newone.unfollow(archer)
		assert_not newone.following(archer)
	end

	test "feed should have the right posts" do
		newone = users(:newone)
		archer = users(:archer)
		lana = users(:lana)
		#posts from followed user
		lana.microposts.each do |post_following|
			assert newone.feed.include?(post_following)
		end
		#post from self
		newone.microposts.each do |post_self|
			assert newone.feed.include?(post_self)
		end
		#post from unfollowed user
		archer.microposts.each do |post_unfollowed|
			assert_not newone.feed.include?(post_unfollowed)
		end
	end

end
