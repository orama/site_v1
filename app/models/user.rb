class User < ApplicationRecord
	has_many :microposts, dependent: :destroy
	has_many :active_relationships, class_name:		"Relationship",
									foreign_key:	"follower_id",
									dependent:		:destroy
	has_many :passive_relationships,  class_name: 	"Relationship",
										foreign_key: "followed_id",
										dependent: 		:destroy
	has_many :following, through: :active_relationships, source: :followed
	has_many :followers, through: :passive_relationships, source: :follower
	attr_accessor :remenber_token, :activation_token
	before_save :downcase_email
	before_create :create_activation_digest
	validates :name, presence: true, length: { maximum: 50 }
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :email, presence: true, length: { maximum: 243 },
			format: {with: VALID_EMAIL_REGEX },
			uniqueness: { case_sensitive: false }
	has_secure_password
	validates :password, presence: true, length: { minimum: 4 }, allow_nil: true


#returns the hash digest of the given string 
	def User.digest(string)
		cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)

	end

#returns a random token
	def User.new_token
		SecureRandom.urlsafe_base64	
	end

#remenber user in db for use in persistent sessions
	def remenber
		self.remenber_token = User.new_token
		update_attribute(:remenber_digest, User.digest(remenber_token))
	end

#return true if the given token matches the digest
	def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
	end

	  # Activates an account.
  def activate
    update_attribute(:activated,    true)
    update_attribute(:activated_at, Time.zone.now)
  end

  # Sends activation email.
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end

#forget user
	def forget
		update_attribute(:remenber_digest, nil)
	end

	def feed
		following_ids = "SELECT followed_id FROM relationships
						WHERE follower_id = :user_id"
		Micropost.where("user_id IN (#{following_ids})
						 OR user_id = :user_id", user_id: id)
	end

	#follow a user maybe change user by something else in orama version
	def follow(other_user)
		following << other_user
	end

	def unfollow(other_user)
		following.delete(other_user)
	end

	def following?(other_user)
		following.include?(other_user)
	end

	private

	def downcase_email
		self.email = email.downcase
	end

	def create_activation_digest
		self.activation_token = User.new_token
		self.activation_digest = User.digest(activation_token)
	end

end
