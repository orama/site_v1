class Link < ApplicationRecord

	belongs_to :category
	# searchkick
	
	def self.search(search)
		where("name LIKE ? OR url LIKE ? OR content LIKE ?", "%#{search}%", "%#{search}%", "%#{search}%")
	end
end
