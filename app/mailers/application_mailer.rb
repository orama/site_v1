class ApplicationMailer < ActionMailer::Base
  default from: "bonsoir@orama.ninja"
  layout 'mailer'
end
