class LinksController < ApplicationController
	
	def index
  		if params[:search]
		 	@links = Link.search(params[:search]).order("created_at DESC")
		else
			@links = Link.all.order('created_at DESC')
		end
	end

	def show
		@link = Link.find(params[:id])
	end

	def new
		@link = Link.new
		@categories = Category.all.map{|c| [ c.name, c.id ] }
	end

	def create
		@link = Link.new(link_params)
		if @link.save
			flash[:success] = "super !"
			redirect_to links_url
		else
			render 'new'
		end
	end 

	def destroy
		Link.find(params[:id]).destroy
		flash[:success] = "post supprimé"
		redirect_to links_url
	end

	 private
    def link_params
      params.require(:link).permit(:name, :url, :content, :img, :category_id)
    end
end
