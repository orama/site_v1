class StaticPagesController < ApplicationController

  def letopo
  end

  def home
    if logged_in?
      render 'landing'
      @micropost = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    else
     render 'landing'
    end
  end

  def fabrique
  end

  def causeries
  end
end
