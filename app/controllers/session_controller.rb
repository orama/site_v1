class SessionController < ApplicationController

  def new
  end

  def create
  	user = User.find_by(email: params[:session][:email])
  	if user && user.authenticate(params[:session][:password])
      if user.activated?
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        redirect_back_or user
      else
        message  = "Compte non activaté. "
        message += "Vérifiez vos emails et cliquez sur le lien d'activation fourni."
        flash[:warning] = message
        redirect_to root_url
      end
  	else
  		flash.now[:danger] = 'mauvais pseudo ou mot de passe'
  		render 'new'
  	end
  end

  def destroy
    log_out if logged_in?
 #   session[:user_id] = nil
    redirect_to root_url, notice: "deco"
  end

end
