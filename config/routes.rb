Rails.application.routes.draw do
  resources :categories
  get 'session/new'

  root                'static_pages#home'
#  get '/home',    to: 'static_pages#home'
  get '/causeries',	to: 'static_pages#causeries'
  get '/letopo',   to: 'static_pages#letopo'
#  get '/links_form', to: 'links#form'
  get '/comment', to: 'static_pages#comment'
#  get '/home', 		to: 'static_pages#home'
  get '/fabrique', 		to: 'static_pages#fabrique'
  get '/signup', 	to: 'users#new'
  post '/signup', to: 'users#create'
#  get '/links',   to: 'links#show'
  post '/links',  to: 'links#create'
  get '/login',		to: 'session#new'
  post '/login',	to: 'session#create'
	delete '/logout',	to: 'session#destroy'
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :account_activations, only: [:edit]
  resources :microposts,         only: [:create, :destroy, :index]
  resources :relationships,       only: [:create, :destroy]
  resources :links
  resources :categories

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
