#config/carrierwave.rb
if Rails.env.test? or Rails.env.development?
  CarrierWave.configure do |config|
    config.storage = :file
    config.root = "#{Rails.root}/tmp"
    config.cache_dir = "#{Rails.root}/tmp/images"
  end
else
  CarrierWave.configure do |config|
    config.fog_credentials = {
      # Configuration for Amazon S3
      :provider              => 'AWS',
      :aws_access_key_id     => ENV['S3_ACCESS_KEY'],
      :aws_secret_access_key => ENV['S3_SECRET_KEY']
    }
    config.fog_directory     =  ENV['S3_BUCKET']
  end
end




# CarrierWave.configure do |config|
#   config.cache_dir = "#{Rails.root}/tmp/uploads"
# end

# if Rails.env.production?
#   CarrierWave.configure do |config|
#     config.fog_credentials = {
#       # Configuration for Amazon S3
#       :provider              => 'AWS',
#       :aws_access_key_id     => ENV['S3_ACCESS_KEY'],
#       :aws_secret_access_key => ENV['S3_SECRET_KEY']
#     }
#     config.fog_directory     =  ENV['S3_BUCKET']
#   end
# end

# ##################################################################
# # This is configuration snippet to configure Google Cloud Platform
# # for fog library.
# #
# # 1. Copy this file into your home dir: "~/.fog":
# #
# #    $ cat .fog.example >> ~/.fog
# #
# # 2. Follow instructions to generate a private key: 
# #    https://cloud.google.com/storage/docs/authentication#generating-a-private-key
# #
# # 3. Edit the new file accordingly.
# #
# ##################################################################
# # START GOOGLE CONFIG
# my_google_credentials:
#     google_project: my-project-id
#     google_client_email: xxxxxxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx@developer.gserviceaccount.com
#     google_json_key_location: /path/to/my-project-xxxxxxxxxxxx.json
#     # You can also provide service account credentials with `google_json_key_string` or 
#     # with `google_key_location` and `google_key_string` for P12 private keys.
#     # If so, uncomment the two following lines.
#     # HMAC credentials follow a similar format:
# 	#google_storage_access_key_id: GOOGXXXXXXXXXXXXXXXX
# 	#google_storage_secret_access_key: XXXX+XXX/XXXXXXXX+XXXXXXXXXXXXXXXXXXXXX
# # /END GOOGLE CONFIG
# #################################################################

# if Rails.env.test? or Rails.env.development?
#   CarrierWave.configure do |config|
#     config.storage = :file
#     config.root = "#{Rails.root}/tmp"
#     config.cache_dir = "#{Rails.root}/tmp/images"
#   end
# else
#   CarrierWave.configure do |config|
#     config.storage = :fog
#         config.fog_provider = 'fog/aws'  
#     config.fog_credentials = {
#       :provider => 'AWS',
#       :aws_access_key_id => ENV['AWS_ACCESS_KEY_ID'],
#       :aws_secret_access_key => ENV['AWS_SECRET_ACCESS_KEY'],
#       :region => ENV['AWS_S3_REGION']
#     }
#     config.fog_directory = ENV['AWS_S3_BUCKET_NAME']
#     config.asset_host = "#{ENV['AWS_S3_ASSET_URL']}/#{ENV['AWS_S3_BUCKET_NAME']}"
#   end
# end


# CarrierWave.configure do |config|
# config.fog_credentials = {

#     :provider                         => 'Google',
#     :google_storage_access_key_id     => Rails.application.secrets.google_storage_access_key_id,
#     :google_storage_secret_access_key => Rails.application.secrets.google_storage_secret_access_key

#     }

#     config.fog_directory = 'your-bucket-name'
# end