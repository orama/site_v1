if defined? Rack::Cors
  Rails.configuration.middleware.insert_before 0, Rack::Cors do
    allow do
      origins %w[
        https://orama.ninja
        https://beta.orama.ninja
        https://salty-scrubland-85867.herokuapp.com
      ]
      resource '/assets/*'
    end
  end
end
