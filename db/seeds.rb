# # This file should contain all the record creation needed to seed the database with its default values.
# # The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
# #
# # Examples:
# #
# #   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
# #   Character.create(name: 'Luke', movie: movies.first)

# User.create!(name:  "Example User",
#              email: "example@railstutorial.org",
#              password:              "foobarmer",
#              password_confirmation: "foobarmer",
#              admin: true,
#              activated: true,
#              activated_at: Time.zone.now)

# #folling relationships
# # users = User.all
# # user = users.first
# # following = users [2..5]
# # followers = users[3..4]
# # following.each { |followed| user.follow(followed)}
# # followers.each { |follower| follower.follow(user)}

require 'csv'

Link.destroy_all

CSV.foreach(Rails.root.join('catalogue_out.csv'), headers: true) do |row|
    Link.create! do |link|
    	link.name = row[0]
    	link.url = row[1]
    	link.content = row[3]
    end
end