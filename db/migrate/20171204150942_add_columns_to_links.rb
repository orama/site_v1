class AddColumnsToLinks < ActiveRecord::Migration[5.1]
  def change
    add_column :links, :category_id, :integer
    add_index :links, :category_id
  end
end
