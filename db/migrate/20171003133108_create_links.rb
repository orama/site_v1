class CreateLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :links do |t|
      t.string :name
      t.string :url
      t.text :content
      t.string :img
      t.references :user, foreign_key: true

      t.timestamps
    end

    #to expand for orama
    add_index :links, [:user_id, :created_at]
  end
end
